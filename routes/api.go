package routes

import (
	"microservices-kwh-meter/routes/version"

	"github.com/gin-gonic/gin"
)

func ApiRoute(route *gin.Engine) {

	version.APIversionV0(route)
}
