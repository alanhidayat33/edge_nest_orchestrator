package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	"golang.org/x/crypto/bcrypt"
)

func RootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name":  "Krisna Wahyuy",
		"kelas": "1 D4 IT A",
	})
}

func SendDataUser(c *gin.Context) {
	conn, _ := amqp.Dial("amqp://guest:guest@localhost:5672/")
	channel, _ := conn.Channel()
	queue, err := channel.QueueDeclare(
		"example-data-user",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Println("error of queue", queue.Name, ":", err)
	}

	type Request struct {
		Name     string `json:"name" binding:"required"`
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	type User struct {
		Name     string
		Email    string
		Password string
	}

	var requestBody Request
	err = c.ShouldBindJSON(&requestBody)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "invalid request body",
			"errors":  err.Error(),
		})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(requestBody.Password), 10)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "failed to hash password",
		})
		return
	}
	newData := User{
		Name:     requestBody.Name,
		Email:    requestBody.Email,
		Password: string(hashedPassword),
	}
	converDataToJson, _ := json.Marshal(newData)

	if err = channel.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        converDataToJson,
		},
	); err != nil {
		fmt.Println("error of queue", queue.Name, ":", err)
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "success to create user",
	})
}
