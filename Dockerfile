# Latest golang image on apline linux
FROM golang:1.20

# Work directory
WORKDIR /docker-go

# Copying go.mod and go.sum
COPY go.mod go.sum ./

# Downloading dependencies
RUN go mod download

# Copying all the files
COPY . .

# Starting our application
CMD ["go", "run", "main.go"]

# Exposing server port
EXPOSE 8800
