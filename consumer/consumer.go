package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"github.com/streadway/amqp"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		fmt.Println("--> error connection to rabbitmq: ", err, "\n")
		panic(err)
	}
	defer conn.Close()
	fmt.Println("--> Successfully connected to rabbitMQ \n")
	channel, err := conn.Channel()
	if err != nil {
		fmt.Println("--> error connection to channel rabbitMQ: ", err)
		panic(err)
	}
	defer channel.Close()
	fmt.Println("--> Successfully connected to channel rabbitMQ \n")
	messages, _ := channel.Consume(
		"example-data-user",
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	// Connect to MongoDB
	clientOptions := options.Client().ApplyURI("mongodb://mongoserver:8888")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.TODO())

	// Select the database and collection
	collection := client.Database("example-data").Collection("user")

	forever := make(chan bool)
	go func() {
		for d := range messages {
			fmt.Printf("Receive : %s \n", d.Body)

			// Parse the message body
			var data map[string]interface{}
			err := json.Unmarshal(d.Body, &data)
			if err != nil {
				log.Fatal(err)
			}

			// Insert the data into the collection
			_, err = collection.InsertOne(context.TODO(), data)
			if err != nil {
				log.Fatal(err)
			}
		}
	}()
	fmt.Println("[*] Waiting message.......")
	<-forever
}
